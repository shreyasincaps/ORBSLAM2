#!/usr/bin/python3
# codec: utf-8

import sys
import matplotlib.pyplot as plt

def main():
    if len(sys.argv) < 3:
        print("usage: ./analyze.py <grep'd logs optimized> <grep'd logs original>")
        return

    map_points_render_times_optimized = []
    kf_graph_render_times_optimized = []
    current_camera_render_times_optimized = []
    with open(sys.argv[1]) as logfile:
        for line in logfile:
            tokens = line.split(" ")
            time_ns = int(tokens[1])

            if "DrawMapPoints" in tokens[0]:
                map_points_render_times_optimized.append(time_ns)
            elif "DrawKeyFrames" in tokens[0]:
                kf_graph_render_times_optimized.append(time_ns)
            elif "DrawCurrentCamera" in tokens[0]:
                current_camera_render_times_optimized.append(time_ns)
    
    map_points_render_times_original = []
    kf_graph_render_times_original = []
    current_camera_render_times_original = []
    with open(sys.argv[2]) as logfile2:
        for line in logfile2:
            tokens = line.split(" ")
            time_ns = int(tokens[1])

            if "DrawMapPoints" in tokens[0]:
                map_points_render_times_original.append(time_ns)
            elif "DrawKeyFrames" in tokens[0]:
                kf_graph_render_times_original.append(time_ns)
            elif "DrawCurrentCamera" in tokens[0]:
                current_camera_render_times_original.append(time_ns)

    print("Average DrawMapPoints render time optimized: {ave}ms".format(ave=sum(map_points_render_times_optimized) / len(map_points_render_times_optimized) / 1000000))
    print("Average DrawMapPoints render time original: {ave}ms".format(ave=sum(map_points_render_times_original) / len(map_points_render_times_original) / 1000000))

    print("Average DrawKeyFrames render time optimized: {ave}ms".format(ave=sum(kf_graph_render_times_optimized) / len(kf_graph_render_times_optimized) / 1000000))
    print("Average DrawKeyFrames render time original: {ave}ms".format(ave=sum(kf_graph_render_times_original) / len(kf_graph_render_times_original) / 1000000))

    print("Average DrawCurrentCamera render time optimized: {ave}ms".format(ave=sum(current_camera_render_times_optimized) / len(current_camera_render_times_optimized) / 1000000))
    print("Average DrawCurrentCamera render time original: {ave}ms".format(ave=sum(current_camera_render_times_original) / len(current_camera_render_times_original) / 1000000))

    print("Max DrawMapPoints render time optimized: {max}ms".format(max=max(map_points_render_times_optimized) / 1000000))
    print("Max DrawMapPoints render time original: {max}ms".format(max=max(map_points_render_times_original) / 1000000))

    print("Max DrawKeyFrames render time optimized: {max}ms".format(max=max(kf_graph_render_times_optimized) / 1000000))
    print("Max DrawKeyFrames render time original: {max}ms".format(max=max(kf_graph_render_times_original) / 1000000))

    print("Max DrawCurrentCamera render time optimized: {max}ms".format(max=max(current_camera_render_times_optimized) / 1000000))
    print("Max DrawCurrentCamera render time original: {max}ms".format(max=max(current_camera_render_times_original) / 1000000))

    print("Min DrawMapPoints render time optimized: {min}ms".format(min=min(map_points_render_times_optimized) / 1000000))
    print("Min DrawMapPoints render time original: {min}ms".format(min=min(map_points_render_times_original) / 1000000))

    print("Min DrawKeyFrames render time optimized: {min}ms".format(min=min(kf_graph_render_times_optimized) / 1000000))
    print("Min DrawKeyFrames render time original: {min}ms".format(min=min(kf_graph_render_times_original) / 1000000))

    print("Min DrawCurrentCamera render time optimized: {min}ms".format(min=min(current_camera_render_times_optimized) / 1000000))
    print("Min DrawCurrentCamera render time original: {min}ms".format(min=min(current_camera_render_times_original) / 1000000))

    cut = min(len(map_points_render_times_optimized), len(map_points_render_times_original))
    plt.plot(map_points_render_times_optimized[:cut])
    plt.plot(map_points_render_times_original[:cut])
    plt.show()

    cut = min(len(kf_graph_render_times_optimized), len(kf_graph_render_times_original))
    plt.plot(kf_graph_render_times_optimized[:cut])
    plt.plot(kf_graph_render_times_original[:cut])
    plt.show()

    cut = min(len(current_camera_render_times_optimized), len(current_camera_render_times_original))
    plt.plot(current_camera_render_times_optimized[:cut])
    plt.plot(current_camera_render_times_original[:cut])
    plt.show()

if __name__ == "__main__":
    main()