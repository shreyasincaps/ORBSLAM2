#ifndef ORB_SLAM_CUSTOM_HANDLER
#define ORB_SLAM_CUSTOM_HANDLER

#include <pangolin/pangolin.h>

class CustomHandler : public pangolin::Handler3D
{
    public:
        CustomHandler(pangolin::OpenGlRenderState& cam_state, pangolin::AxisDirection enforce_up=pangolin::AxisNone, float trans_scale=0.01f, float zoom_fraction=PANGO_DFLT_HANDLER3D_ZF);

        virtual void MouseMotion(pangolin::View&, int x, int y, int button_state);
};

#endif // ORB_SLAM_CUSTOM_HANDLER

