/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "MapDrawer.h"
#include "MapPoint.h"
#include "KeyFrame.h"
#include <pangolin/pangolin.h>
#include <mutex>

namespace ORB_SLAM2
{


MapDrawer::MapDrawer(Map* pMap, const string &strSettingPath):mpMap(pMap)
{
    cv::FileStorage fSettings(strSettingPath, cv::FileStorage::READ);

    mKeyFrameSize = fSettings["Viewer.KeyFrameSize"];
    mKeyFrameLineWidth = fSettings["Viewer.KeyFrameLineWidth"];
    mGraphLineWidth = fSettings["Viewer.GraphLineWidth"];
    mPointSize = fSettings["Viewer.PointSize"];
    mCameraSize = fSettings["Viewer.CameraSize"];
    mCameraLineWidth = fSettings["Viewer.CameraLineWidth"];

}

void MapDrawer::DrawMapPoints()
{
    const vector<MapPoint*> &vpMPs = mpMap->GetAllMapPoints();
    const vector<MapPoint*> &vpRefMPs = mpMap->GetReferenceMapPoints();

    set<MapPoint*> spRefMPs(vpRefMPs.begin(), vpRefMPs.end());

    if(vpMPs.empty())
        return;

    // Prepare the data on the CPU.
    auto start = std::chrono::high_resolution_clock::now();

    auto number_of_points_max = (vpMPs.size() + vpRefMPs.size()) * 3;
    std::vector<float> data_position(number_of_points_max), data_colors(number_of_points_max);

    std::size_t i {0};
    for (const auto &mp : vpMPs) {
        if(mp->isBad() || spRefMPs.count(mp))
            continue;

        const auto &world_pos = mp->GetWorldPos();

        // Position.
        data_position[i * 3] = world_pos.at<float>(0);
        data_position[i * 3 + 1] = world_pos.at<float>(1);
        data_position[i * 3 + 2] = world_pos.at<float>(2);

        // Color.
        data_colors[i * 3] = 0;
        data_colors[i * 3 + 1] = 0;
        data_colors[i * 3 + 2] = 0;

        ++i;
    }

    for (const auto &mp : vpRefMPs) {
        if(mp->isBad())
            continue;

        const auto &world_pos = mp->GetWorldPos();

        // Position.
        data_position[i * 3] = world_pos.at<float>(0);
        data_position[i * 3 + 1] = world_pos.at<float>(1);
        data_position[i * 3 + 2] = world_pos.at<float>(2);

        // Color.
        data_colors[i * 3] = 1;
        data_colors[i * 3 + 1] = 0;
        data_colors[i * 3 + 2] = 0;

        ++i;
    }

    auto delta = std::chrono::high_resolution_clock::now() - start;
    auto delta_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(delta);
    std::cout << "\t[DrawMapPoints-A] " << delta_ns.count() << " ns" << std::endl;

    // Make the VBO and the CBO.
    start = std::chrono::high_resolution_clock::now();

    pangolin::GlBuffer
        vbo(pangolin::GlArrayBuffer, i, GL_FLOAT, 3, GL_STATIC_DRAW),
        cbo(pangolin::GlArrayBuffer, i, GL_FLOAT, 3, GL_STATIC_DRAW);

    delta = std::chrono::high_resolution_clock::now() - start;
    delta_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(delta);
    std::cout << "\t[DrawMapPoints-B] " << delta_ns.count() << " ns" << std::endl;

    // Upload data to VBOs.
    start = std::chrono::high_resolution_clock::now();

    vbo.Upload(static_cast<const GLvoid *>(data_position.data()), i * 3 * sizeof(float));
    cbo.Upload(static_cast<const GLvoid *>(data_colors.data()), i * 3 * sizeof(float));

    delta = std::chrono::high_resolution_clock::now() - start;
    delta_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(delta);
    std::cout << "\t[DrawMapPoints-C] " << delta_ns.count() << " ns" << std::endl;

    // Render the VBO/CBO.
    start = std::chrono::high_resolution_clock::now();

    pangolin::RenderVboCbo(vbo, cbo, true, GL_POINTS);

    delta = std::chrono::high_resolution_clock::now() - start;
    delta_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(delta);
    std::cout << "\t[DrawMapPoints-D] " << delta_ns.count() << " ns" << std::endl;
}

void MapDrawer::DrawKeyFrames(const bool bDrawKF, const bool bDrawGraph)
{
    const float &w = mKeyFrameSize;
    const float h = w*0.75;
    const float z = w*0.6;
    std::size_t i {0};
    std::vector<float> data_lines, data_colors;
    data_lines.reserve(10000);
    data_colors.reserve(10000);



    const vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();

    if(bDrawKF)
    {
   	
	for (size_t i=0; i<vpKFs.size(); i++) {
	
		KeyFrame* pKF = vpKFs[i];
            	cv::Mat Twc = pKF->GetPoseInverse().t();

            	glPushMatrix();

            	glMultMatrixf(Twc.ptr<GLfloat>(0));
        
        	
               data_lines.insert(data_lines.end(),{0,0,0,w,h,z,0,0,0,w,-h,z,0,0,0,-w,-h,z,0,0,0,-w,h,z,w,h,z,w,-h,z,w,h,z,-w,-h,z,-w,h,z,w,h,z,-w,-h,z,w,-h,z});
               data_colors.insert(data_colors.end(),{0.0,0.0,1.0,0.0});
               
               glPopMatrix();
        } 
        

    }
    
    

    if(bDrawGraph)
    {
       

        
        for(size_t j=0; j<vpKFs.size(); j++)
        {
            // Covisibility Graph
            const vector<KeyFrame*> vCovKFs = vpKFs[j]->GetCovisiblesByWeight(100);

            cv::Mat Ow = vpKFs[j]->GetCameraCenter();
        
            if(!vCovKFs.empty()){

                for(vector<KeyFrame*>::const_iterator vit=vCovKFs.begin(), vend=vCovKFs.end(); vit!=vend; vit++)
                    {
                        if((*vit)->mnId<vpKFs[j]->mnId)
                            continue;
                        cv::Mat Ow2 = (*vit)->GetCameraCenter();
                        data_lines.insert(data_lines.end(),{0,0,0,world_pos.at<float>(0),world_pos.at<float>(1),world_pos.at<float>(2)});
                        data_colors.insert(data_colors.end(),{0.0,1.0,0.0,0.6});
           

                }
            }



            KeyFrame* pParent = vpKFs[j]->GetParent();
            if(pParent)
            {
                cv::Mat Owp = pParent->GetCameraCenter();
                data_lines.pushback(Ow.at<float>(0));
                data_lines.pushback(Ow.at<float>(1));
                data_lines.pushback(Ow.at<float>(2));
                data_lines.pushback(Owp.at<float>(0));
                data_lines.pushback(Owp.at<float>(1));
                data_lines.pushback(Owp.at<float>(2));
                data_colors.pushback(0.0);
                data_colors.pushback(1.0);
                data_colors.pushback(0.0);
                data_colors.pushback(0.6);
            }
            

            set<KeyFrame*> sLoopKFs = vpKFs[i]->GetLoopEdges();
            for(set<KeyFrame*>::iterator sit=sLoopKFs.begin(), send=sLoopKFs.end(); sit!=send; sit++)
            {
                if((*sit)->mnId<vpKFs[i]->mnId)
                    continue;
                cv::Mat Owl = (*sit)->GetCameraCenter();
                data_lines.pushback(Ow.at<float>(0));
                data_lines.pushback(Ow.at<float>(1));
                data_lines.pushback(Ow.at<float>(2));
                data_lines.pushback(Owl.at<float>(0));
                data_lines.pushback(Owl.at<float>(1));
                data_lines.pushback(Owl.at<float>(2));
                data_colors.pushback(0.0);
                data_colors.pushback(1.0);
                data_colors.pushback(0.0);
                data_colors.pushback(0.6);
                
            }
    
       }
  }
  
  
  	pangolin::GlBuffer
        vbo(pangolin::GlArrayBuffer, data_lines.size(), GL_FLOAT, 3, GL_STATIC_DRAW),
        cbo(pangolin::GlArrayBuffer, data_colors.size(), GL_FLOAT, 3, GL_STATIC_DRAW);

        

        vbo.Upload(static_cast<const GLvoid *>(data_lines.data()), data_lines.size() * sizeof(float));
        cbo.Upload(static_cast<const GLvoid *>(data_colors.data()), data_colors.size() * sizeof(float));

        glLineWidth(mKeyFrameLineWidth);
        pangolin::RenderVboCbo(vbo, cbo, true, GL_LINES);





}

void MapDrawer::DrawCurrentCamera(pangolin::OpenGlMatrix &Twc)
{
    const float &w = mCameraSize;
    const float h = w*0.75;
    const float z = w*0.6;
    std::vector<float> data_lines, data_colors;
    data_lines.reserve(1000);
    data_colors.reserve(1000);


    glPushMatrix();

#ifdef HAVE_GLES
        glMultMatrixf(Twc.m);
#else
        glMultMatrixd(Twc.m);
#endif
    data_lines.insert(data_lines.end(),{0,0,0,w,h,z,0,0,0,w,-h,z,0,0,0,-w,-h,z,0,0,0,-w,h,z,w,h,z,w,-h,z,w,h,z,-w,-h,z,-w,h,z,w,h,z,-w,-h,z,w,-h,z});
    data_colors.insert(data_colors.end(),{0.0,1.0,0.0,0.0});

    pangolin::GlBuffer
    vbo(pangolin::GlArrayBuffer, data_lines.size(), GL_FLOAT, 3, GL_STATIC_DRAW),
    cbo(pangolin::GlArrayBuffer, data_colors.size(), GL_FLOAT, 3, GL_STATIC_DRAW);

        

    vbo.Upload(static_cast<const GLvoid *>(data_lines.data()), data_lines.size() * sizeof(float));
    cbo.Upload(static_cast<const GLvoid *>(data_colors.data()), data_colors.size() * sizeof(float));

    glLineWidth(mKeyFrameLineWidth);
    pangolin::RenderVboCbo(vbo, cbo, true, GL_LINES);

    glPopMatrix();
}


void MapDrawer::SetCurrentCameraPose(const cv::Mat &Tcw)
{
    unique_lock<mutex> lock(mMutexCamera);
    mCameraPose = Tcw.clone();
}

void MapDrawer::GetCurrentOpenGLCameraMatrix(pangolin::OpenGlMatrix &M)
{
    if(!mCameraPose.empty())
    {
        cv::Mat Rwc(3,3,CV_32F);
        cv::Mat twc(3,1,CV_32F);
        {
            unique_lock<mutex> lock(mMutexCamera);
            Rwc = mCameraPose.rowRange(0,3).colRange(0,3).t();
            twc = -Rwc*mCameraPose.rowRange(0,3).col(3);
        }

        M.m[0] = Rwc.at<float>(0,0);
        M.m[1] = Rwc.at<float>(1,0);
        M.m[2] = Rwc.at<float>(2,0);
        M.m[3]  = 0.0;

        M.m[4] = Rwc.at<float>(0,1);
        M.m[5] = Rwc.at<float>(1,1);
        M.m[6] = Rwc.at<float>(2,1);
        M.m[7]  = 0.0;

        M.m[8] = Rwc.at<float>(0,2);
        M.m[9] = Rwc.at<float>(1,2);
        M.m[10] = Rwc.at<float>(2,2);
        M.m[11]  = 0.0;

        M.m[12] = twc.at<float>(0);
        M.m[13] = twc.at<float>(1);
        M.m[14] = twc.at<float>(2);
        M.m[15]  = 1.0;
    }
    else
        M.SetIdentity();
}

} //namespace ORB_SLAM
