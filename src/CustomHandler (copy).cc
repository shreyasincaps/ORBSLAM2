#include "CustomHandler.h"
using namespace pangolin;

CustomHandler::CustomHandler(pangolin::OpenGlRenderState& cam_state, pangolin::AxisDirection enforce_up, float trans_scale, float zoom_fraction)
    : pangolin::Handler3D(cam_state, enforce_up, trans_scale, zoom_fraction) {

}

void CustomHandler::MouseMotion(pangolin::View& display, int x, int y, int button_state) {


    const GLprecision rf = 0.01;
    const float delta[2] = { (float)x - last_pos[0], (float)y - last_pos[1] };
    const float mag = delta[0]*delta[0] + delta[1]*delta[1];

    if((button_state & KeyModifierCtrl) && (button_state & KeyModifierShift))
    {
        GLprecision T_nc[3 * 4];
        LieSetIdentity(T_nc);

        GetPosNormal(display, x, y, p, Pw, Pc, n, last_z);
        if(ValidWinDepth(p[2]))
        {
            last_z = p[2];
            std::copy(Pc, Pc + 3, rot_center);
        }

        funcKeyState = button_state;
    }
    else
    {
        funcKeyState = 0;
    }

    // TODO: convert delta to degrees based of fov
    // TODO: make transformation with respect to cam spec
    if( mag < 50.0f*50.0f )
    {
        OpenGlMatrix& mv = cam_state->GetModelViewMatrix();
        const GLprecision* up = AxisDirectionVector[enforce_up];
        GLprecision T_nc[3*4];
        LieSetIdentity(T_nc);
        bool rotation_changed = false;
        
        if( button_state == MouseButtonLeft )
        {
            // Left Drag: in plane translate
            if( ValidWinDepth(last_z) )
            {
                GLprecision np[3];
                PixelUnproject(display, x, y, last_z, np);
                const GLprecision t[] = { np[0] - rot_center[0], np[1] - rot_center[1], 0};
                LieSetTranslation<>(T_nc,t);
                std::copy(np,np+3,rot_center);
            }else{
                const GLprecision t[] = { -10*delta[0]*tf, 10*delta[1]*tf, 0};
                LieSetTranslation<>(T_nc,t);
            }
        }else if( button_state == (MouseButtonLeft | MouseButtonRight) )
        {
            // Left and Right Drag: in plane rotate about object
            //        Rotation<>(T_nc,0.0,0.0, delta[0]*0.01);
            
            GLprecision T_2c[3*4];
            Rotation<>(T_2c, (GLprecision)0.0, (GLprecision)0.0, delta[0]*rf);
            GLprecision mrotc[3];
            MatMul<3,1>(mrotc, rot_center, (GLprecision)-1.0);
            LieApplySO3<>(T_2c+(3*3),T_2c,mrotc);
            GLprecision T_n2[3*4];
            LieSetIdentity<>(T_n2);
            LieSetTranslation<>(T_n2,rot_center);
            LieMulSE3(T_nc, T_n2, T_2c );
            rotation_changed = true;
        }
        /*
        else if( button_state == MouseButtonRight)
        {
            GLprecision aboutx = -rf * delta[1];
            GLprecision abouty = -rf * delta[0];

            // Try to correct for different coordinate conventions.
            if(cam_state->GetProjectionMatrix().m[2*4+3] <= 0) {
                abouty *= -1;
            }
            
            if(enforce_up) {
                // Special case if view direction is parallel to up vector
                const GLprecision updotz = mv.m[2]*up[0] + mv.m[6]*up[1] + mv.m[10]*up[2];
                if(updotz > 0.98) aboutx = std::min(aboutx, (GLprecision)0.0);
                if(updotz <-0.98) aboutx = std::max(aboutx, (GLprecision)0.0);
                // Module rotation around y so we don't spin too fast!
                abouty *= (1-0.6*fabs(updotz));
            }
            
            // Right Drag: object centric rotation
            GLprecision T_2c[3*4];
            Rotation<>(T_2c, aboutx, abouty, (GLprecision)0.0);
            GLprecision mrotc[3];
            MatMul<3,1>(mrotc, rot_center, (GLprecision)-1.0);
            LieApplySO3<>(T_2c+(3*3),T_2c,mrotc);
            GLprecision T_n2[3*4];
            LieSetIdentity<>(T_n2);
            LieSetTranslation<>(T_n2,rot_center);
            LieMulSE3(T_nc, T_n2, T_2c );
            rotation_changed = true;
        }
        */
        LieMul4x4bySE3<>(mv.m,T_nc,mv.m);
        
        if(enforce_up != AxisNone && rotation_changed) {
            EnforceUpT_cw(mv.m, up);
        }
    }
    
    last_pos[0] = (float)x;
    last_pos[1] = (float)y;

}

